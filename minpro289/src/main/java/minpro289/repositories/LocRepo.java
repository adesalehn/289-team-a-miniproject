package minpro289.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import minpro289.models.M_location;
@Repository
public interface LocRepo extends JpaRepository<M_location, Long> {

}
