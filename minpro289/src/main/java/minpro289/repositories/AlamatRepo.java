package minpro289.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import minpro289.models.M_biodata_address;

@Repository
public interface AlamatRepo extends JpaRepository<M_biodata_address, Long> {

	@Query("FROM M_biodata_address Where Location_id=?1")
	List<M_biodata_address> findByLocation_id(Long Location_id);
}
