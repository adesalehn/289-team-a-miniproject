package minpro289.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import minpro289.models.M_location;
import minpro289.repositories.LocRepo;

@Controller
@CrossOrigin("*")
@RequestMapping(value="/location/")
public class LocController {
	
	@Autowired LocRepo locrepo;

	@GetMapping("data")
    public ResponseEntity<List<M_location>> getAllSpes(){
        try {
            List<M_location> loc = this.locrepo.findAll();
            return new ResponseEntity<>(loc, HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

}
