package minpro289.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import minpro289.models.M_biodata_address;
import minpro289.models.M_location;
import minpro289.repositories.AlamatRepo;
import minpro289.repositories.LocRepo;



@RestController
@CrossOrigin("*")
@Controller
@RequestMapping(value="/alamat/")
public class AlamatController {

	@Autowired
	private AlamatRepo alamatrepo;
	
	@Autowired
	private LocRepo locrepo;
	
	@GetMapping(value="index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("/alamat/index");
		List<M_biodata_address> listalamat = this.alamatrepo.findAll();
		view.addObject("listalamat", listalamat);
		return view;
	}
	
	@GetMapping(value="form")
	public ModelAndView form() {
		ModelAndView view = new ModelAndView("/alamat/form");
		M_biodata_address alamat = new M_biodata_address();
		view.addObject("alamat", alamat);
		List<M_location> location = this.locrepo.findAll();
		view.addObject("location", location);
		return view;
	}
	
	@PostMapping(value="save")
	public ModelAndView save(@ModelAttribute M_biodata_address m_biodata_address, BindingResult result) {
		if(!result.hasErrors()) {
			this.alamatrepo.save(m_biodata_address);
		}
		return new ModelAndView("redirect:/alamat/index");
		}
	
	@GetMapping(value="/delete/{id}")
	public ModelAndView deleteform(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("/alamat/deleteform");
		M_biodata_address m_biodata_address = this.alamatrepo.findById(id).orElse(null);
		view.addObject("m_biodata_address", m_biodata_address);
		return view;
	}
	
	@GetMapping(value="/del/{id}")
	public ModelAndView del(@PathVariable("id") Long id) {
		if(id != null) {
			this.alamatrepo.deleteById(id);
		}
		
		return new ModelAndView ("redirect:/alamat/index");
	}
	
	@GetMapping("/getnamebylocid/{id}")
	public ResponseEntity<?>getnamebylocid(@PathVariable("id") Long id){
		try {
			List<M_biodata_address>location = this.alamatrepo.findByLocation_id(id);
			if(location != null) {
				return new ResponseEntity<>(location,HttpStatus.OK);
			} else {
				return ResponseEntity
						.status(HttpStatus.NOT_FOUND)
						.body("LocationId dengan id" + id + "tidak ada");
			}
		} catch(Exception e) {
			return new ResponseEntity<M_biodata_address>(HttpStatus.NO_CONTENT);
		}
	}
	
}
